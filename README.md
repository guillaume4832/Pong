**Pong**
===================


Ce projet est un clone de Pong fait avec l'éditeur de jeux [Superpowers](http://superpowers-html5.com) avec la technologie [TypeScript](http://www.typescriptlang.org/)


## Licence ##
Le jeu est distribuée sous licence MIT avec des ressources sous licence CC BY
## Fonctionnalités ##

 - Multiplate-forme
 - Tactile
 - Optimisé pour l'HTML 5

## Exécution ##

 1. Récupérer le dépôt avec la commande ```git clone https://framagit.org/guillaume4832/Pong.git ``` ou en téléchargeant l'archive
 2. Extraire l'archive
 3. Aller dans le dossier /HTML5
 4. Lancer sur un navigateur web index.html
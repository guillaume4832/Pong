class ComportementsBehavior extends Sup.Behavior {
  awake() {
    
  }

  update() {
    
  }
}
Sup.registerBehavior(ComportementsBehavior);

const VITBALLE = 3; // Vitesse de base de la balle

let scoreJ1 = 0;
let scoreJ2 = 0;
let ecran = Sup.Input.getScreenSize();
let activeJ1 =false;


function Tactile(num : number) {
    //Sup.log("tactile " + this.actor.getY());
      let ydoigtJ = Sup.Input.getTouchPosition(num).y;
      let proportion= ((Sup.Input.getTouchPosition(num).y +1)/2);
      let nvposy = proportion*ecran.y;
      return nvposy;
}

class CmpJ1 extends Sup.Behavior { // Comportement du J1
  awake() {
    //Sup.log("awake " + this.actor.getY());
    
  }

  update() {
    //Sup.log("update " + this.actor.getY());
    let velJ1= this.actor.arcadeBody2D.getVelocity(); // Récupère la vélocité du palet J1
    velJ1.y = 0;
    
    if(Sup.Input.isKeyDown("SHIFT") && this.actor.getY() < 670){
      velJ1.y=10;
    }
   ////////////////////////////////Tactile
    if(Sup.Input.wasTouchStarted(0)){
      activeJ1 = true;
    }else if (Sup.Input.wasTouchEnded(0)){
      activeJ1 =false;
    }
    //Sup.log("start :"+ Sup.Input.wasTouchStarted(0)+ " end : " + Sup.Input.wasTouchEnded(0));
    if ( activeJ1 && (Sup.Input.getTouchPosition(0).x <= 0 ) ) {
      let posyJ1 = Tactile(0);
      this.actor.setY(posyJ1);
      
    }
    /////////////////////////////////
    if(Sup.Input.isKeyDown("CONTROL") && this.actor.getY() > 50){
      velJ1.y=-10;
    }
    //Sup.log("3 " + posyJ1);
    this.actor.arcadeBody2D.setVelocity(velJ1);
    this.actor.arcadeBody2D.warpPosition(this.actor.getX(),this.actor.getY());// équivaut au Return velJ1
  }                                             // ne pas oublier de l'appliquer à l'objet
}
Sup.registerBehavior(CmpJ1);

class CmpJ2 extends Sup.Behavior { // Comportement du J2
  awake() {
    
  }

  update() {
    let velJ2= this.actor.arcadeBody2D.getVelocity(); // Récupère la vélocité du palet J2
    velJ2.y = 0;
    
    if(Sup.Input.isKeyDown("UP") && this.actor.getY() < 670){
      velJ2.y=10;
    }
    if(Sup.Input.isKeyDown("DOWN") && this.actor.getY() > 50){
      velJ2.y=-10;
    }
       ////////////////////////////////Tactile
    if ((Sup.Input.wasTouchStarted(1) || !Sup.Input.wasTouchEnded(1)) && (Sup.Input.getTouchPosition(1).x > 0 ) ) {
      let posyJ2 = Tactile(1);
      this.actor.setY(posyJ2);
      
    }
    /////////////////////////////////
    this.actor.arcadeBody2D.setVelocity(velJ2); // équivaut au Return velJ2
  }                                             // ne pas oublier de l'appliquer à l'objet
}
Sup.registerBehavior(CmpJ2);

class CmpBalle extends Sup.Behavior { // Comportement de la balle
  
  vitesse = VITBALLE; // vitesse de la balle
  sx = 1; // Sens de la balle à l'horizontal
  sy = 1; // Sens de la balle à la verticale
  
  awake() {
    
  }

  update() {
    let velBalle = this.actor.arcadeBody2D.getVelocity();
    
    if(Sup.ArcadePhysics2D.collides(this.actor.arcadeBody2D,Sup.ArcadePhysics2D.getAllBodies())){ // cogne une raquette
      this.sx = this.sx * -1;
      this.vitesse = this.vitesse + 1;
    }
    
    if(this.actor.getY() > 710 || this.actor.getY() < 10 ){ // cogne un mur
      this.sy = this.sy * -1;
    }
    
    if(this.actor.getX() > 1280){
      this.actor.arcadeBody2D.warpPosition(new Sup.Math.Vector3(640,360,0)); // Quand gain de point, remise balle au centre
      this.vitesse = VITBALLE;
      this.sx = this.sx * -1;
      scoreJ1 = scoreJ1 + 1;
    }
    
    if(this.actor.getX() < 0){
      this.actor.arcadeBody2D.warpPosition(new Sup.Math.Vector3(640,360,0)); // Quand gain de point, remise balle au centre
      this.vitesse = VITBALLE;
      this.sx = this.sx * -1;
      scoreJ2 = scoreJ2 + 1;
    }
    
    velBalle.x = this.vitesse * this.sx;
    velBalle.y = this.vitesse * this.sy;
    
    this.actor.arcadeBody2D.setVelocity(velBalle);
  }
}
Sup.registerBehavior(CmpBalle);

class CmpScore extends Sup.Behavior {
  awake() {
    
  }

  update() {
    this.actor.textRenderer.setText(scoreJ1 + " : " + scoreJ2);
    
  }
}
Sup.registerBehavior(CmpScore);

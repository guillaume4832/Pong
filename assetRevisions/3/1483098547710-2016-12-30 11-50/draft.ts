class ComportementsBehavior extends Sup.Behavior {
  awake() {
    
  }

  update() {
    
  }
}
Sup.registerBehavior(ComportementsBehavior);

class CmpJ1 extends Sup.Behavior { // Comportement du J1
  awake() {
    
  }

  update() {
    let velJ1= this.actor.arcadeBody2D.getVelocity(); // Récupère la vélocité du palet J1
    velJ1.y = 0;
    
    if(Sup.Input.isKeyDown("SHIFT")){
      velJ1.y=10;
    }
    if(Sup.Input.isKeyDown("CONTROL")){
      velJ1.y=-10;
    }
    this.actor.arcadeBody2D.setVelocity(velJ1); // équivaut au Return velJ1
  }
}
Sup.registerBehavior(CmpJ1);


/*class CmpBalle extends Sup.Behavior {
  awake() {
    
  }

  update() {
    
  }
}
Sup.registerBehavior(CmpBalle);*/
